<?php

/**
 * @package Plugin HTTP2 Server Push for Joomla! 3.4+
 * @version 1.1.1 2016-07-04 02:33 by horza
 * @author Davor Grubisa
 * @copyright (C) 2016 - Davor Grubisa
 * @license GNU/GPLv2 https://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
**/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

class plgSystemHttp2PushInstallerScript {

	/**
	* Called on installation
	*
	* @param   JAdapterInstance  $adapter  The object responsible for running this script
	*
	* @return  boolean  True on success
	*/
	function install($adapter) {
	}

	/**
	* Called on uninstallation
	*
	* @param   JAdapterInstance  $adapter  The object responsible for running this script
	*/
	function uninstall($adapter) {
		//echo '<p>'. JText::_('1.6 Custom uninstall script') .'</p>';
	}

	/**
	* Called on update
	*
	* @param   JAdapterInstance  $adapter  The object responsible for running this script
	*
	* @return  boolean  True on success
	*/
	function update($adapter) {
		//echo '<p>'. JText::_('1.6 Custom update script') .'</p>';
	}

	/**
	* Called before any type of action
	*
	* @param   string  $route  Which action is happening (install|uninstall|discover_install)
	* @param   JAdapterInstance  $adapter  The object responsible for running this script
	*
	* @return  boolean  True on success
	*/
	function preflight($route, $adapter) {
		//echo '<p>'. JText::sprintf('1.6 Preflight for %s', $route) .'</p>';
	}

	/**
	* Called after any type of action
	*
	* @param   string  $route  Which action is happening (install|uninstall|discover_install)
	* @param   JAdapterInstance  $adapter  The object responsible for running this script
	*
	* @return  boolean  True on success
	*/
	function postflight($route, $adapter) {
		
		if ($route=='update') {
			$oldfolders = array();
			foreach ($oldfolders as $oldfolder) {
				if (JFolder::exists($oldfolder))
					JFolder::delete($oldfolder);
			}
	
			$oldfiles = array();
			
			foreach ($oldfiles as $oldfile) {
				if (JFile::exists($oldfile))
					JFile::delete($oldfile);
			}
		}
		
		if ($route=='install' || $route=='update') {
			$lang = JFactory::getLanguage();
			$lang->load('plg_system_http2push',JPATH_SITE.'/plugins/system/http2push');
			$url = 'index.php?option=com_plugins&filter_search=Push';
	
			$plugin_name = JText::_('PLG_SYSTEM_HTTP2PUSH');
			?>
			<div class="well clearfix">
				<h2><img src="../plugins/system/http2push/assets/images/appstore48.png" width="48" height="48" alt="<?php echo $plugin_name; ?>"/>&nbsp; <?php echo $plugin_name; ?></h2>
				<p class="lead">Plugin installed</p>
				<div class="row-fluid">
					<a class="btn btn-large btn-primary pull-left span5" href="<?php echo $url; ?>"><?php echo JText::_('PLG_HTTP2PUSH_CONFIGURE'); ?></a>
					<a href="https://www.horza.org/" target="new" class="pull-right span5"><img src="../plugins/system/http2push/assets/images/daycounts.png" style="" alt="horza.org"/></a>
				</div>
			</div>
			<br />
			<?php
		}
	}
}