<?php
/**
 * @package Plugin HTTP2 Server Push for Joomla! 3.4+
 * @version 1.1.1 2016-07-04 02:33 by horza
 * @author Davor Grubisa
 * @copyright (C) 2016 - Davor Grubisa
 * @license GNU/GPLv2 https://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
**/

defined('JPATH_BASE') or die();

class JFormFieldTitle extends JFormField {

	protected $type = 'Title';

	public function getInput()	{

		$description = $this->description;
		$description = (JText::_($description)) ? JText::_($description) : $description;

		$html = '';
		if ($this->value) {
			$html .= '<div style="margin: 10px 0 5px 0; font-weight: bold; padding: 5px; background-color: #cacaca; float:none; clear:both;">';
			$html .= JText::_($this->value);
			$html .= '</div>';
			$html .= $description;
		}
		
		return $html;
	}	
	
}
