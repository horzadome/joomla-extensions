<?php

/**
 * @package Plugin HTTP2 Server Push for Joomla! 3.4+
 * @version 1.1.1 2016-07-04 02:33 by horza
 * @author Davor Grubisa
 * @copyright (C) 2016 - Davor Grubisa
 * @license GNU/GPLv2 https://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
**/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');
jimport('joomla.environment.browser');
jimport('joomla.filesystem.file');

class plgSystemHttp2Push extends JPlugin
{

	public function __construct(& $subject, $params = array())
	{
		parent::__construct( $subject, $params );
	}

	function onBeforeCompileHead()
	{
		error_reporting(E_ALL);
		$app = JFactory::getApplication('site');
		if ( $app->isAdmin()) return; //Exit if in administration
		$doc = JFactory::getDocument();
		
		$scripts_to_handle = trim( (string) $this->params->get('scripts_to_handle', ''));
		$css_to_handle = trim( (string) $this->params->get('css_to_handle', ''));
                $images_to_handle = trim( (string) $this->params->get('images_to_handle', ''));

// PROCESS JAVASCRIPT	
                if ($scripts_to_handle) {
                        $paths = array_map('trim', (array) explode("\n", $scripts_to_handle));
                        $debug = '';
                        $preloads = '';
                        
                        if ($this->params->get('debug')) {
                                $lang = JFactory::getLanguage();
                                $lang->load('plg_system_http2push',JPATH_SITE.'/plugins/system/http2push');
                                $debug .= '<ul><h3>'.JText::_('PLG_HTTP2PUSH_SCRIPTS_TO_FIND').':</h3>';
                                foreach ($paths as $url) {
                                        $debug .= '<li>'.$url.'</li>';
                                }
                                $debug .= '</ul>';
                                $debug .= '<ul><h3>'.JText::_('PLG_HTTP2PUSH_SCRIPTS_FOUND').':</h3>';
                        }
                        
                        
			foreach ($paths as $path) {

				if (strpos($path,'http')===0) {
					continue;
				}
				
				$path = trim($path);

				//Get the path only
				$uri = JUri::getInstance($path);
				$pathonly = $uri->toString(array('path'));
				if ($pathonly != $path) {
					$paths[] = $pathonly;
				}
			}

		
			foreach ($paths as $url) {

				//Get the path only
				$searchUrl = trim($url);
				$uri = JUri::getInstance($searchUrl);
                                $searchUrl = "/".ltrim ($uri->toString(array('path')), "//");
                        

				if (in_array($searchUrl,$paths)) {
					if ($this->params->get('serverpush')) {
						$debug .= '<li>'.$url.' ==> <span class="label label-success">HTTP2 server pushed</span></li>';
						$preloadurl = ltrim($url, "/");
						$preloads .= '</'.$preloadurl.'>; rel=preload; as=script,';
					}
				} else {
					$debug .= '<li>'.$url.' ==> <span class="label label-important">SKIP</span></li>';
				}
			}
						foreach ($doc->_scripts as $url => $scriptparams) {

				//Get the path only
				$searchUrl = trim($url);
				$uri = JUri::getInstance($searchUrl);
                                $searchUrl = "/".ltrim ($uri->toString(array('path')), "//");
                        

				if (in_array($searchUrl,$paths)) {
					if ($this->params->get('serverpush')) {
					}
				} else {
					$debug .= '<li>'.$url.' ==> <span class="label label-warning">Found but NOT pushed</span></li>';
				}
			}
		}

// PROCESS CSS	
                if ($css_to_handle) {
                        $paths = array_map('trim', (array) explode("\n", $css_to_handle));
                        //$debug = '';
                        //$preloads = '';
                        
                        if ($this->params->get('debug')) {
                                $lang = JFactory::getLanguage();
                                $lang->load('plg_system_http2push',JPATH_SITE.'/plugins/system/http2push');
                                $debug .= '<ul><h3>'.JText::_('PLG_HTTP2PUSH_CSS_TO_FIND').':</h3>';
                                foreach ($paths as $url) {
                                        $debug .= '<li>'.$url.'</li>';
                                }
                                $debug .= '</ul>';
                                $debug .= '<ul><h3>'.JText::_('PLG_HTTP2PUSH_CSS_FOUND').':</h3>';
                        }
                        
                        
			foreach ($paths as $path) {

				if (strpos($path,'http')===0) {
					continue;
				}
				
				$path = trim($path);

				//Get the path only
				$uri = JUri::getInstance($path);
				$pathonly = $uri->toString(array('path'));
				if ($pathonly != $path) {
					$paths[] = $pathonly;
				}
			}

			foreach ($paths as $url) {

				//Get the path only
				$searchUrl = trim($url);
				$uri = JUri::getInstance($searchUrl);
				$searchUrl = "/".ltrim ($uri->toString(array('path')), "//");                      

				if (in_array($searchUrl,$paths)) {
					if ($this->params->get('serverpush')) {
						$debug .= '<li>'.$url.' ==> <span class="label label-success">HTTP2 server pushed</span></li>';
						$preloadurl = ltrim($url, "/");
						$preloads .= '</'.$preloadurl.'>; rel=preload; as=style,';
					}
				} else {
					$debug .= '<li>'.$url.' ==> <span class="label label-warning">SKIP</span></li>';
				}
			}
			
			foreach ($doc->_styleSheets as $url => $scriptparams) {
				//Get the path only
				$searchUrl = trim($url);
				$uri = JUri::getInstance($searchUrl);
				$searchUrl = "/".ltrim ($uri->toString(array('path')), "//");                      
				if (in_array($searchUrl,$paths)) {
					if ($this->params->get('serverpush')) {
					}
				} else {
					$debug .= '<li>'.$url.' ==> <span class="label label-warning">Found but NOT pushed</span></li>';
				}
			}

		}		
// PROCESS IMAGES	
                if ($images_to_handle) {
                        $paths = array_map('trim', (array) explode("\n", $images_to_handle));
                        //$debug = '';
                        //$preloads = '';
                        
                        if ($this->params->get('debug')) {
                                $lang = JFactory::getLanguage();
                                $lang->load('plg_system_http2push',JPATH_SITE.'/plugins/system/http2push');
                                $debug .= '<ul><h3>'.JText::_('PLG_HTTP2PUSH_IMAGES_TO_FIND').':</h3>';
                                foreach ($paths as $url) {
                                        $debug .= '<li>'.$url.'</li>';
                                }
                                $debug .= '</ul>';
                                $debug .= '<ul><h3>'.JText::_('PLG_HTTP2PUSH_IMAGES_FOUND').':</h3>';
                        }
                        
                        
			foreach ($paths as $path) {

				if (strpos($path,'http')===0) {
					continue;
				}
				
				$path = trim($path);

				//Get the path only
				$uri = JUri::getInstance($path);
				$pathonly = $uri->toString(array('path'));
				if ($pathonly != $path) {
					$paths[] = $pathonly;
				}
			}

		
			foreach ($paths as $url) {

				//Get the path only
				$searchUrl = trim($url);
				$uri = JUri::getInstance($searchUrl);
				$searchUrl = "/".ltrim ($uri->toString(array('path')), "//");                      

				if (in_array($searchUrl,$paths)) {
					if ($this->params->get('serverpush')) {
						$debug .= '<li>'.$url.' ==> <span class="label label-success">HTTP2 server pushed</span></li>';
						$preloadurl = ltrim($url, "/");
						$preloads .= '</'.$preloadurl.'>; rel=preload; as=image,';
					}
				} else {
					$debug .= '<li>'.$url.' ==> <span class="label label-warning">SKIP</span></li>';
				}
			}

		}
		
		$app = JFactory::getApplication();
                $linkheader = rtrim($preloads, ",");
                $app->setHeader('Link', $linkheader, false);

                if ($this->params->get('debug')) {
                        $debug .= '</ul>';
                        $app->enqueueMessage($debug,'http2 Server Push DEBUG');
                }
		
		return true;
	}
	
}